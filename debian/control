Source: rust-users
Section: rust
Priority: optional
Build-Depends: debhelper (>= 12),
 dh-cargo (>= 24),
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-libc-0.2+default-dev <!nocheck>,
 librust-log-0.4+default-dev <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Sylvestre Ledru <sylvestre@debian.org>,
 kpcyrd <git@rxv.cc>
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/users]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/users
Rules-Requires-Root: no

Package: librust-users-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-libc-0.2+default-dev,
 librust-log-0.4+default-dev
Recommends:
 librust-users+default-dev (= ${binary:Version})
Provides:
 librust-users+cache-dev (= ${binary:Version}),
 librust-users+logging-dev (= ${binary:Version}),
 librust-users+mock-dev (= ${binary:Version}),
 librust-users-0-dev (= ${binary:Version}),
 librust-users-0+cache-dev (= ${binary:Version}),
 librust-users-0+logging-dev (= ${binary:Version}),
 librust-users-0+mock-dev (= ${binary:Version}),
 librust-users-0.11-dev (= ${binary:Version}),
 librust-users-0.11+cache-dev (= ${binary:Version}),
 librust-users-0.11+logging-dev (= ${binary:Version}),
 librust-users-0.11+mock-dev (= ${binary:Version}),
 librust-users-0.11.0-dev (= ${binary:Version}),
 librust-users-0.11.0+cache-dev (= ${binary:Version}),
 librust-users-0.11.0+logging-dev (= ${binary:Version}),
 librust-users-0.11.0+mock-dev (= ${binary:Version})
Description: Accessing Unix users and groups - Rust source code
 This package contains the source for the Rust users crate, packaged by debcargo
 for use with cargo and dh-cargo.

Package: librust-users+default-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-users-dev (= ${binary:Version}),
 librust-users+cache-dev (= ${binary:Version}),
 librust-users+mock-dev (= ${binary:Version}),
 librust-users+logging-dev (= ${binary:Version})
Provides:
 librust-users-0+default-dev (= ${binary:Version}),
 librust-users-0.11+default-dev (= ${binary:Version}),
 librust-users-0.11.0+default-dev (= ${binary:Version})
Description: Accessing Unix users and groups - feature "default"
 This metapackage enables feature "default" for the Rust users crate, by pulling
 in any additional dependencies needed by that feature.
